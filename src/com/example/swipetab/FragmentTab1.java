package com.example.swipetab;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.app.Fragment;

public class FragmentTab1 extends android.support.v4.app.Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
      
          View rootView = inflater.inflate(R.layout.fragmenttab1, container, false);
       
        return rootView;
    }
 
}