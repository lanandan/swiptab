package com.example.swipetab;

import android.support.v4.view.ViewPager;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import com.example.swipetab.MyAdapter;

import android.os.Bundle;

@SuppressWarnings("deprecation")
public class MainActivity extends ActionBarActivity implements android.support.v7.app.ActionBar.TabListener{
ActionBar actionbar;
ViewPager vi;
MyAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		 vi=(ViewPager)findViewById(R.id.pager11);
		actionbar=getSupportActionBar();
		actionbar.setHomeButtonEnabled(false);
		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionbar.addTab(actionbar.newTab().setText("fruits").setIcon(R.drawable.fruits).setTabListener(this));
		actionbar.addTab(actionbar.newTab().setText("birds").setIcon(R.drawable.birds).setTabListener(this));
		actionbar.addTab(actionbar.newTab().setText("World").setIcon(R.drawable.world).setTabListener(this));
		actionbar.addTab(actionbar.newTab().setText("fruits").setIcon(R.drawable.babelfish).setTabListener(this));
		adapter=new MyAdapter(getSupportFragmentManager());
		vi.setAdapter(adapter);
		vi.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				actionbar.setSelectedNavigationItem(arg0);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	public void onTabReselected(android.support.v7.app.ActionBar.Tab arg0,
			android.support.v4.app.FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabSelected(android.support.v7.app.ActionBar.Tab arg0,
			android.support.v4.app.FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		vi.setCurrentItem(arg0.getPosition());
		
		
	}

	@Override
	public void onTabUnselected(android.support.v7.app.ActionBar.Tab arg0,
			android.support.v4.app.FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}
	
	

	
}
